FROM busybox:1 as download

ARG LOVD_VERSION=3.0-23

RUN wget "https://www.lovd.nl/3.0/download.php?version=${LOVD_VERSION}&type=tar.gz" -O lovd.tar.gz && \
    mkdir lovd && \
    tar xvzf lovd.tar.gz --strip-components=2 -C lovd


FROM php:7-apache-buster

WORKDIR /var/www/html/

COPY --from=download /lovd ./

RUN apt update && apt install -y --no-install-recommends \
      libxml2-dev && \
    rm -rf /var/lib/apt/lists/* && \
    docker-php-ext-install pdo pdo_mysql soap && \
    a2enmod rewrite

COPY docker-entrypoint /usr/local/bin/

ENTRYPOINT ["docker-entrypoint"]

CMD ["apache2-foreground"]
