# lovd

Docker image for LOVD - Leiden Open Variation Database https://www.lovd.nl

This builds a docker image for LOVD and an init container image that initializes LOVD so there is no need to follow the manual installation steps after the deployment.
The default configuration can be found at `init-lovd/lovd-db-installation.sql`

A kubernetes deployment of LOVD with MySQL can be found at `lovd-k8s.yaml`

Available tags:\
registry.gitlab.com/precmed/deployment/lovd:latest\
registry.gitlab.com/precmed/deployment/lovd:3.0-23\
registry.gitlab.com/precmed/deployment/lovd:3.0-23-7-apache-buster
